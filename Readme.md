## Ubuntu 20.04 firefox webdriver docker image with XRDP
Fully implemented Multi User xrdp, modified from https://github.com/danielguerra69/ubuntu-xrdp-docker.git
Users can re-login in the same session.
Xfce4, Firefox(with webdriver) are pre installed.

## To run with docker-compose

```bash
docker-compose up -d
```
